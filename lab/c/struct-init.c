
#include<stdio.h>

typedef struct ex {
    int a;
    int b;
} ex;

int main(){
    ex obj[2] = {
        [0] = { 5, 88 },
        [1] = { 6, 98 },
    };

    for(int i=0;i<sizeof(obj)/sizeof(ex); i++){
        printf("%d, %d\n", obj[i].a, obj[i].b);
    }  
}
