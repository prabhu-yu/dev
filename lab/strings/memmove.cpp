#include<iostream>
/*
 g++ memmove.cpp && ./a.out
 */

using std::cout;

void *memmove(void *dest1, void *src1, size_t n) {
    char *dest = (char*)dest1;
    char *src = (char*)src1;

    if (src < dest){
        cout << "case 1\n";
        dest = (char *)dest + n -1;
        src = (char *)src + n - 1;
        while(n--){
            *dest-- = *src--;
        }
    } else {
        while (n--){
            *dest++ = *src++;   
        }    
    }
    return dest;
}

int main(int argc, char *argv[]){
    char src[10] {"hello"};
    char dest[10];

    memmove(dest, src, 10);
    cout << src << "\n";    
    cout << dest << "\n";    
}

