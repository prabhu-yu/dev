/*
 g++ strstr.cpp && ./a.out
 */
#include<iostream>
using std::cout;

const int compare(const char *str, const char *substring){
    while(*str && *substring && (*str == *substring)){
        str++, substring++;    
    }
    
    if(!*substring){
        return 0;
    }
    return 1;
}

const char *strstr(const char *str, const char *substr){
    
    while(*str){
        if(!compare(str, substr)){
            return str;
        }
        str++;
    }

    return nullptr;
}

int main(int argc, char *argv[]){
    
    const char *str = "hello new brave world!";
    const char *substr = "brave  world";

    const char *ptr = strstr(str, substr);
    cout << "ptr=" << ptr;

    return 0;
}
