#!/usr/bin/env python3
# https://en.wikiversity.org/wiki/Python_Concepts/Bytes_objects_and_Bytearrays

"""
convert a string to bytes type.
"""
L = []
str = 'abcdef'
for i in str:
    L.append(ord(i))

b = bytes(L)
print(b)

s1 = 'ウ キペディア' # 7 japanese chars
s1 = 'hello' # 7 japanese chars
en = s1.encode('utf-8', errors='ignore')
print(f'{s1=}')
print(f'{en=}')
print(f'{len(s1)=} Vs {len(en)=}')