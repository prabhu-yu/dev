//https://nandland.com/introduction-to-verilog-for-beginners-with-code-examples/

module example_and_gate(ip1, ip2, and_result);
    input ip1;
    input ip2;
    output and_result;

    wire and_temp;
    assign and_temp = ip1 & ip2;
    assign and_result = and_temp;
endmodule

