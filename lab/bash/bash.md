
# bash tutorial
## shell re-directions

how to redirect both stdout ans stderr to single file?
[stack overflow](https://stackoverflow.com/questions/818255/what-does-21-mean)


the re-direct operator > expecpts on its RHS side a file_path or file_descripto.
file_descriptors(aka fd) are integer numbers and indicated by & before them.
for ex: &1 is stdout fd, &2 is stderr fd. 
file_path are pure file paths that we see.


look below code.
ls 2>&1 1>file 
2 is pointing to 1, which is /dev/tty0
1 is pointing to file.
Effect is:
only 1 goes to file, 2 still comes to /dev/tty0
Generally, this is not what we wanted.


look at below code.(this we want most of the time)
ls 1>file 2>&1
first stdout starts pointing to file.
then stderr starts pointing to stdout, which is nothing but file.
so, both 1,2 goes to file.
This is so often used, there is a shorthand &> in bash.
but this shorthand is supported in POSIX shell. so,
avoid using it. also, short hands conflicts with csh. so just aviod it.


## tee command with redirect.

ls | tee output.txt # only stdout is passed to output.txt
ls 2>&1 | tee output.txt # both fd: 1 and 2 goes to tee.
for this, first 2 should be pointing to 1, then pipe takes 1 to tee.




